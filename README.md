# LiBuC v02

LInk Builder Client v2.1.1 for SEO purposes: a dummy sites network



TODO:
- check auth methods
- clean code
- improve empty cache generation
- create new themes


------------------------------------------------------------
V2.1.1  (2023-05-09)
- movede config files outside of the directory
- changed directory folder structure
- removed warnings if no terms exists

------------------------------------------------------------
V2.1 New version! (2023-04-18)
- Added Pages support
- A lot of Themes improvements
- Added Webform support
- The only themes that support v02 features are
	- alpha

------------------------------------------------------------
V0.7.3 New Theme added (2020-06-10)
 - new Delta theme

------------------------------------------------------------
V0.7.2 New Themes + fixes (2020-06-09)
 - new Newspaper theme
 - beta theme updated
 - new GTM integration
 - some fixes on config.php.sample

------------------------------------------------------------
V0.7.1 Fixes (2020-06-08) - New GA tag for all three themes

------------------------------------------------------------
V0.7 Improvements (2020-06-08) - fixes + beta theme Added


------------------------------------------------------------
V0.6.5 Improvements (2020-06-04) - fix path errors


------------------------------------------------------------
V0.6.4 Improvements (2020-06-04) - create cache if does not exists
 - check if cache files exists
 - create empty cache files in cache does not exists
 - force download of new cache setting a very old modified date


------------------------------------------------------------
V0.6.3 Improvements (2020-06-04) - handle empty folder
 - handle empty folder installation path in config.php


------------------------------------------------------------
V0.6.1 Improvements (2020-06-04) - remove config from git
 - remove config file


------------------------------------------------------------
V0.6.2 Improvements (2020-06-04) - add cache files
 - Added Cache dir


------------------------------------------------------------
V0.6.1 Improvements (2020-06-04) - Alfa theme added
 - Added Alfa theme by Luca


------------------------------------------------------------
V0.6 Improvements (2020-06-03) - Source authentication
 - LiBuS: auth in REST views + content types (Articles, websites)
 - use $context to send basic auth to sewrver
 - moved username and password in settings


------------------------------------------------------------
V0.5 Improvements (2020-05-18) - Cookie banner & Privacy Policy

 - Websites: added fields on LiBuS (use cookie banner, cookie bannertxt, privacy policy url, cookie policy url)
 - Basic theme: added privacy policy link (if exist)
 - Basic theme: added cookie policy link (if exist)
 - Basic theme: added cookie banner with fade effects (jquery cookie)
 - Basic theme: added cookie banner txt
 - Basic theme: added cookie banner with cookie tho hide or show previous choose


------------------------------------------------------------
V0.4 Improvements (2020-05-11)

- Articles: <title> tag using "SEO title field" and fall back on "title field"
- Articles: Meta description tag using "SEO description" and fall back on "body"
- Homepage: <title> tag using site title field
- Homepage: Meta description site description
- Category: <title> tag using site title field and category title
- Categort: Meta description usign category body

- Added sitemap generator

- Articles: added related articles (internal cross navigation)
