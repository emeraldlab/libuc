<?php
//define('__ROOT__', dirname(dirname(__FILE__)));
$basepath = getcwd();
require_once $basepath.'/init.php';

if ($site->debug == true){

}
else{
  $alerts = '';
}

// Load Twig
require_once $basepath.'/vendor/autoload.php';
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
$loader = new FilesystemLoader($basepath . '/themes/');
//$twig = new Environment($loader);
$twig = new \Twig\Environment($loader, [
    'debug' => true
]);
$twig->addExtension(new HelloNico\Twig\DumpExtension());


$pathJs = substr($Path->level0, 0, 6);

if ($Path->level0 == 'sitemap.xml'){
  require_once $basepath.'/sitemap.php';
}
elseif ($Path->level0 == 'robots.txt'){
  require_once $basepath.'/robots.php';
}
elseif ($pathJs == 'jshead'){
  http_response_code(200);
  header('Content-type: text/javascript');
  if ($AnalyticsGtmHead->id){
    echo $AnalyticsGtmHead->rendered;
  }
  if ($AnalyticsGaHead->id){
    echo $AnalyticsGaHead->rendered;
  }
  if ($AnalyticsMatomoScript->id){
    echo $AnalyticsMatomoScript->rendered;
  }
  if ($AnalyticsMatomoContainer->id){

    echo $AnalyticsMatomoContainer->rendered;
  }
}
else{
  echo $twig->render($SiteData->theme.'/base.html.twig',
      [
      'themefolder'=> $SiteData->theme,
      'sitedata' => $SiteData,
      'articles' => $SiteArticles,
      'terms'=> $SiteTerms,
      'pages'=> $SitePages,
      'page'=> $CurrentPage,
      'path'=> $Path,
      'alerts' => $alerts,
      ]
  );
}

?>
