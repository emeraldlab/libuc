<?php

// Init alerts array
$alerts = array();

// Init Alert object
class Alert {
  public $message;
  public $severity;
}
class WebSites {}
class CurrentPage {}

$newalert = new Alert();
$CurrentPage = new CurrentPage();


// Include config and libraries
require_once $basepath.'/../config.php';
require_once $basepath.'/libs/functions.php';
require_once $basepath.'/libs/alerts.php';

require_once $basepath.'/libs/navigation.php';
require_once $basepath.'/libs/getdata.php';

pushAlert('Inclusi  /config.php e /libs/functions.php','info');


// Init Error 404
class ErrorPage {
  public $code;
  public $title;
  public $body;
}

$Error404 = new ErrorPage();
require_once $basepath.'/themes/_assets/error_pages.php';

// Init Analytics Snippets
class AnalyticsSnippet{
  public $id;
  public $endpoint;
  public $renered;
}

//prevent endpoint donwload if cache is not expired
$lastcacheTimestamp = json_decode(file_get_contents('./cache/CheckUpdates.json'))->timestamp;
$nowTimestamp = time();
$validcacheTimestamp = $lastcacheTimestamp+$site->maxCache;

if ($validcacheTimestamp > $nowTimestamp){
  pushAlert('La cache è valida: scade '.$validcacheTimestamp,'info');

  $data = new stdClass();
  $data->website = loadDataFromCache('website');
  $data->terms = loadDataFromCache('terms');
  $data->articles = loadDataFromCache('articles');
  $data->pages = loadDataFromCache('pages');

}else{
  pushAlert('La cache NON è valida, è scaduta il '.$validcacheTimestamp,'info');
  $data = checkUpdates($site);
}




// Check data from server
#$data = checkUpdates($site);
$SiteData = $data->website;
$SiteData->theme = $site->theme;
$SiteData->section_prefix = $site->section_prefix;



if (!empty($data->terms)){
  $SiteTerms = $data->terms;
}else{
  $SiteTerms = false;
}
if (!empty($data->articles)){
  $SiteArticles = $data->articles;
}else{
  $SiteArticles = false;
}
if (!empty($data->pages)){
  $SitePages = $data->pages;
}
else{
  $SitePages = false;
}

$Path = siteNavigation($SiteTerms,$SiteArticles,$SitePages);

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
    $baseurl= "https";
}
else{
    $baseurl = "http";
}

if ($site->forcehttps){
  $baseurl= "https";
}

$baseurl .= "://";
$baseurl .= $_SERVER['HTTP_HOST'];

// Init Routing
if ($site->installdir == ''){
  $SiteData->baseurl = $baseurl;
}
else{
  $SiteData->baseurl =  $baseurl.'/'.$site->installdir;
}

// Webform
$SiteData->webformdebug = $site->webformdebug;
$SiteData->webformendpoit = $site->webformendpoit;

$SiteData->webformokfeedback = $site->webformokfeedback;
$SiteData->webformmessageok = $site->webformmessageok;
$SiteData->webformmessageko = $site->webformmessageko;
$SiteData->webformredirect = $site->webformredirect;

// Show Other Websites
$SiteData->show_otherwebsites = $site->show_otherwebsites;

// Analytics - Matomo
$site->MTMid = $SiteData->mtm_id;
$site->MTMtag = $SiteData->mtm_tag;
$AnalyticsMatomoScript = new AnalyticsSnippet();
$AnalyticsMatomoContainer = new AnalyticsSnippet();


// Analytics - Google TAG Manager
$site->GTMid = $SiteData->gtm;
$AnalyticsGtmHead = new AnalyticsSnippet();
//$AnalyticsGtmBody = new AnalyticsSnippet();


// Analytics - Google GA4 Manager
$site->GAid = $SiteData->ga;
$AnalyticsGaHead = new AnalyticsSnippet();
//$AnalyticsGaBody = new AnalyticsSnippet();


require_once $basepath.'/themes/_assets/scripts_analytics.php';

$Scripts = new stdClass();
$Scripts->gtmhead = $AnalyticsGtmHead;
$Scripts->gtmbody = $AnalyticsGtmBody;
$Scripts->matomo = $AnalyticsMatomoScript;
$Scripts->matomotag = $AnalyticsMatomoContainer;


?>
