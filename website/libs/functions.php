<?php


function checkUpdates($site)
{

    // Object to return
    $data = new stdClass();

    // Cache
    $cacheFile = './cache/CheckUpdates.json';

    // Enpoints
    $lastUpdatedWebsiteUrl = $site->datasrc.$site->id.'/lastupdated/website';
    $lastUpdatedArticleUrl = $site->datasrc.$site->id.'/lastupdated/article';
    $lastUpdatedPageUrl = $site->datasrc.$site->id.'/lastupdated/page';

    // Get data
    $lastUpdatedWebsite=file_get_contents($lastUpdatedWebsiteUrl, false, $site->apiAuth);
    $lastUpdatedArticle=file_get_contents($lastUpdatedArticleUrl, false, $site->apiAuth);
    $lastUpdatedPage=file_get_contents($lastUpdatedPageUrl, false, $site->apiAuth);


    if (!$lastUpdatedArticle OR !$lastUpdatedWebsite OR !$lastUpdatedPage) {

      // CASE unable to reach remote API
      $error = error_get_last();
      pushAlert('<strong>Impossibile recuperare una delle risorse remote</strong>. Copia di Cache in uso. <br/>Errore: '.$error['message'],'danger');

      $data->website = loadDataFromCache('website');
      $data->terms = loadDataFromCache('terms');
      $data->articles = loadDataFromCache('articles');
      $data->pages = loadDataFromCache('pages');

    } else {

      // CASE able to reach remote API
      pushAlert('<strong>Remote API:</strong> il valore di $lastUpdatedArticle è  '.$lastUpdatedArticle,'primary');
      pushAlert('<strong>Remote API:</strong> il valore di $lastUpdatedWebsite è  '.$lastUpdatedWebsite,'primary');
      pushAlert('<strong>Remote API:</strong> il valore di $lastUpdatedPage è  '.$lastUpdatedPage,'primary');

      // Init ad populate updates object
      $lastUpdated = new stdClass();
      $lastUpdated->timestamp = time();
      $lastUpdated->website = json_decode($lastUpdatedWebsite)[0];

      if (!empty(json_decode($lastUpdatedArticle)))
      {
        $lastUpdated->article = json_decode($lastUpdatedArticle)[0];
      }
      if (!empty(json_decode($lastUpdatedPage)))
      {
      $lastUpdated->page = json_decode($lastUpdatedPage)[0];
      }


      // Cache exists - load data from cache
      if (loadDataFromCache('checkupdates')){

        pushAlert('<strong>$cacheFile caricato da '.$cacheFile.' </strong>. Il file esiste!','success');
        $cacheData = loadDataFromCache('checkupdates');
        $cacheTimestamp = $cacheData->timestamp;
        $nowTimestamp = time();
        $expiringTime = $site->maxCache;
        $expiredTimestamp = $cacheTimestamp+$expiringTime;

        if ($expiredTimestamp<$nowTimestamp){

          // La cache è scaduta!
          $elapsed = $expiredTimestamp-$nowTimestamp;
          pushAlert('<strong>La cache è scaduta da '.$elapsed.' secondi.</strong> Devo verificare le risorse remote','info');


          //  WEBSITE
          $cachedLastModifiedWebsite = $cacheData->website->changed;
          $apiLastModifiedWebsite = $lastUpdated->website->changed;


          pushAlert('$cachedLastModifiedWebsite ha valore: '.$cachedLastModifiedWebsite,'info');
          pushAlert('$apiLastModifiedWebsite ha valore: '.$apiLastModifiedWebsite,'info');

          if ($cachedLastModifiedWebsite<$apiLastModifiedWebsite){

            //Carico dati remoti;
            pushAlert('Devo scaricare i dati remoti','primary');
            $data->website =  getSite($site);
            writeDataToCache('website',$data->website);


            // Aggiorno Last Updated
            $cacheData->website->changed = $lastUpdated->website->changed;
            $cacheData->timestamp = time();
            file_put_contents($cacheFile, json_encode($cacheData));

          }else{
            // Load data from cache
            pushAlert('Devo usare i dati della cache','primary');
            $data->website = loadDataFromCache('website');
            $cacheData->timestamp = time();
            file_put_contents($cacheFile, json_encode($cacheData));
          }

          if ($cacheData->article){


            $cachedLastModifiedArticle = $cacheData->article->changed;
            $apiLastModifiedArticle =$lastUpdated->article->changed;

            if ($cachedLastModifiedArticle<$apiLastModifiedArticle){
              //Carico dati remoti;
              $data->articles =  getArticles($site);
              writeDataToCache('articles',$data->articles);

              // Aggiorno Last Updated
              $cacheData->article->changed = $lastUpdated->article->changed;
              $cacheData->timestamp = time();
              file_put_contents($cacheFile, json_encode($cacheData));

              // Scrico le nuove categorie
              $data->terms = getTerms ($site);
              writeDataToCache('terms',$data->terms);

            }else{
              // Load data from cache
              $data->articles = loadDataFromCache('articles');
              $data->terms = loadDataFromCache('terms');
              $cacheData->timestamp = time();
              file_put_contents($cacheFile, json_encode($cacheData));
            }
          }
          else{

          }


          //  PAGES
          if ($cacheData->page && $lastUpdated->page){

            $cachedLastModifiedPage = $cacheData->page->changed;
            $apiLastModifiedPage =$lastUpdated->page->changed;

            if ($cachedLastModifiedPage<$apiLastModifiedPage){
              //Carico dati remoti;
              $data->pages =  getPages($site);
              writeDataToCache('pages',$data->pages);

              // Aggiorno Last Updated
              $cacheData->page->changed = $lastUpdated->page->changed;
              $cacheData->timestamp = time();
              file_put_contents($cacheFile, json_encode($cacheData));


            }else{
              // Load data from cache
              $data->pages = loadDataFromCache('pages');
              $cacheData->timestamp = time();
              file_put_contents($cacheFile, json_encode($cacheData));
            }
          }



        }else{

          $remaining = $expiredTimestamp-$nowTimestamp;
          pushAlert('<strong>La cache è valida ancora per '.$remaining.' secondi.</strong> Uso i dati in cache.','info');

          $data->website = loadDataFromCache('website');
          $data->terms = loadDataFromCache('terms');
          $data->articles = loadDataFromCache('articles');
          $data->pages = loadDataFromCache('pages');
        }
      }


      // No Cache - No party: load data from remote, save it in cache and use it
      else{

        pushAlert('<strong>'.$cacheFile.': NON esiste!</strong>','warning');
        file_put_contents($cacheFile, json_encode($lastUpdated));

        pushAlert('<strong>'.$cacheFile.'</strong>: è stato creato!','success');

        // TODO: fai scaricare tutto
        $data->website = getSite($site);
        writeDataToCache('website',$data->website);
        $data->terms = getTerms($site);
        writeDataToCache('terms',$data->terms);
        $data->articles = getArticles($site);
        writeDataToCache('articles',$data->articles);
        $data->pages = getPages($site);
        writeDataToCache('pages',$data->pages);

      }

    }

    return $data;
}


function loadDataFromCache($type){

  $data = new stdClass();

  if ($type == 'website'){
    $file = './cache/SiteData.json';
    $default = '{"id":0,"title":"Dummy site","field_website_base_url":"","created":"2000-01-01T01:01:01+02:00","changed":"2000-01-01T01:01:01+02:00","ga":"","favicon":"","body":"","logo":"","color_primary":"#ff0000","color_secondary":"#eeeeee","color_tertiary":"#eeeeff","footer":"","network_websites":[{}],"cookiebanner":true,"cookiebanner_txt":"","url_cookie":"","url_privacy":""}';
  }

  else if ($type == 'terms'){
    $file = './cache/Terms.json';
    $default = '{}';
  }

  else if ($type == 'checkupdates'){
    $file = './cache/CheckUpdates.json';
    $default = '{"timestamp":0000000000,"website":{"changed":"2000-01-01T01:01:01+02:00","site_id":1,"title":"Empty website"},"article":{"changed":"2000-01-01T01:01:01+02:00"},"page":{"changed":"2000-01-01T01:01:01+02:00"}}';
  }

  else if ($type == 'articles'){
    $file = './cache/Articles.json';
    $default = '{"nid":0,"path":"empty","title":"empty","cover_url_original":"","cover_url":"","cover_alt":"","body":"test","created":"2000-01-01T01:01:01+02:00","changed":"2000-01-01T01:01:01+02:00","website":"Default","website_id":"","links":[],"categories":[]}';
  }

  else if ($type == 'pages'){
    $file = './cache/Pages.json';
    $default = '{}';
  }


  if (file_exists($file)) {
    if (file_get_contents($file)){
      pushAlert('Il file  '.$file.' esiste','success');
      $data = file_get_contents($file);
    }
    else{
      pushAlert('Il file  '.$file.' è vuoto','warning');
      file_put_contents($file, $default);
      $data = $default;
    }

  } else {
      pushAlert('Il file  '.$file.' NON esiste','warning');
      file_put_contents($file, $default);
      $data = $default;
  }

  $return = json_decode($data);

  return $return;
}



function writeDataToCache($type,$data){

  if ($type == 'website'){
    $cacheFile = './cache/SiteData.json';
  }
  else if ($type == 'terms'){
    $cacheFile = './cache/Terms.json';
  }

  else if ($type == 'articles'){
    $cacheFile = './cache/Articles.json';
  }

  else if ($type == 'pages'){
    $cacheFile = './cache/Pages.json';
  }

  file_put_contents($cacheFile, json_encode($data));
}



 ?>
