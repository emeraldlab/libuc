<?php


function getSite($site)
{
    $file = './cache/SiteData.json'; // Cache
    $getdataurl = $site->datasrc.$site->id.'/website'; // Build remote server url

    //$RemoteFile=file_get_contents($getdataurl, false, $site->apiAuth);
    $RemoteFile=file_get_contents($getdataurl, false, $site->apiAuth);

    //$RemoteFile=file_get_contents($getdataurl); // Get data from remote server

    if (!$RemoteFile) {
      pushAlert('Impossibile recuperare la risorsa remota: SiteData da '.$getdataurl,'danger');

      $CacheData = json_decode(file_get_contents($file));
      return $CacheData[0];

    } else {
      $RemoteData=json_decode($RemoteFile);
      $RemoteDataUpdated = $RemoteData[0]->changed;

      return $RemoteData[0];
    }
}



function getTerms($site)
{
    $file = './cache/Terms.json'; // Cache
    $getdataurl = $site->datasrc.$site->id.'/categories'; // Build remote server url


    $RemoteData=json_decode(file_get_contents($getdataurl, false, $site->apiAuth));

    // No caching sistem at all. TODO!!!
    foreach ($RemoteData as &$Term) {
        $type = 'category';
        $Term->path = substr($Term->path , 1);
    }
    return $RemoteData;
}



function getArticles($site){

  pushAlert('<strong>eseguo getArticles</strong>','info');

  global $SiteTerms;

  $file = './cache/Articles.json'; // Cache
  $getdataurl = $site->datasrc.$site->id.'/articles'; // Build remote server url
  //$RemoteFile=file_get_contents($getdataurl); // Get data from remote server
  $RemoteFile=file_get_contents($getdataurl, false, $site->apiAuth);

  if (!$RemoteFile) {
    pushAlert('<strong>getArticles</strong> -  Impossibile recuperare la risorsa remota: Articles da '.$getdataurl,'error');
    $CacheData = json_decode(file_get_contents($file)); // Open the file to get Cached Data
    return $CacheData;

  } else {
    pushAlert('<strong>getArticles</strong> - Risorsa remota scaricata: Articles scaricati da '.$getdataurl,'success');
    $RemoteData=json_decode($RemoteFile);
    return $RemoteData;
  }
}


function getPages($site){


  $file = './cache/Pages.json'; // Cache
  $getdataurl = $site->datasrc.$site->id.'/pages'; // Build remote server url
  $RemoteFile=file_get_contents($getdataurl, false, $site->apiAuth);

  if (!$RemoteFile) {
    pushAlert('<strong>getPages</strong> -  Impossibile recuperare la risorsa remota: Pages da '.$getdataurl,'error');

    $CacheData = json_decode(file_get_contents($file)); // Open the file to get Cached Data
    return $CacheData;

  } else {
    pushAlert('<strong>getPages</strong> - Risorsa remota scaricata: Pages scaricati da '.$getdataurl,'success');
    $RemoteData=json_decode($RemoteFile);
    return $RemoteData;
  }
}

?>
