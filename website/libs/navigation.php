<?php
function siteNavigation($SiteTerms,$SiteArticles){

  global $alerts;
  global $site;
  global $Error404;

  // Init path Object
  $path = new stdClass();
  $currentUrl = explode("?", $_SERVER['REQUEST_URI']);
  $currentUri = $currentUrl[0];
  $currentQuery = $currentUrl[1];

  //pushAlert('Current URI is: '.$currentUri,'info');


  $currentPath = str_replace( '/'.$site->installdir.'/', "", $currentUri);  //pushAlert('Current PATH is: '.$currentPath,'info');
  if (substr( $currentPath, 0, 1 ) === "/"){
    $currentPath = substr($currentPath, 1);
  }

  $currentPathArray = explode("/", $currentPath);

  // Get path depth and structure
  $pathLevelsCount = count($currentPathArray);
  if ( $currentPathArray[0] == '' ||  $currentPathArray[0] == 'home'){
      $pathLevelsCount = 0;
  }

  pushAlert('<strong>Path level count:</strong> '.$pathLevelsCount,'info');

  switch ($pathLevelsCount) {
    case 1:
      $path->level0 = $currentPathArray[0];
      break;
    case 2:
      $path->level0 = $currentPathArray[0];
      $path->level1 = $currentPathArray[1];
      break;
    case 3:
      $path->level0 = $currentPathArray[0];
      $path->level1 = $currentPathArray[1];
      $path->level2 = $currentPathArray[3];
      break;
    default:
      $path->level0 = '';
      $path->level1 = '';
      $path->level2 = '';
      break;
    }


  // Serve the correct page type depending from the path

  if ($pathLevelsCount == 0){
      $type = 'homepage';
  }
  else if ($path->level0 == 'pages'){
      $type = 'page';
      global $SitePages;
      global $CurrentPage;


      foreach ($SitePages as $Page) {
        $exploedPAgePath = explode("/", $Page->path);
        $pagePath =  end($exploedPAgePath);
        if ($path->level1 == $pagePath){
          $CurrentPage = $Page;
        }

      }
  }

  else if ($pathLevelsCount == 1){
    $type = 'category';
    $articlecount = 0;
    $Articles = [];

    if ($SiteTerms){
      foreach ($SiteTerms as $Term) {

        if ($Term->path == $path->level0){
          $path->content = $Term;
          $TermId = $Term->id;

          foreach ($SiteArticles as $Article) {

            foreach ($Article->categories as $Category) {

              if ($Category->id == $TermId){
                array_push($Articles, $Article);
                $articlecount++;
              }
              else{

              }
            }
          }
          $path->articles = $Articles;
        }
      }
    }
    pushAlert('Trovati: '.$articlecount.' articoli apparenenti alla categoria','info');

    if ($articlecount == 0){

      pushAlert('NESSUN articolo trovato per la categoria','warning');
      $type = 'article';
      http_response_code($Error404->code);
      array_push($Articles, $Error404);
      $articlecount++;
      $path->content = $Error404;

    }
  }

  else if ($pathLevelsCount == 2){
    $type = 'article';
    $articlepath = '/'.$currentPath;
    $articlecount = 0;
    //pushAlert('Questo è di sicuro un articolo','info');
    pushAlert('valore da confrontare $articlepath: '.$articlepath,'info');
    foreach ($SiteArticles as $Article) {
      if ($Article->path == $articlepath){
        pushAlert('Articolo trovato: '.$articlepath,'info');
        $path->content = $Article;
        $articlecount++;
      }
      else{

      }
    }
    pushAlert('Trovati: '.$articlecount.' articoli con il percorso corretto','info');
    if ($articlecount == 0){
      pushAlert('NESSUN articolo trovato','warning');
      http_response_code($Error404->code);
      $path->content = $Error404;
    }
  }

  else{
    $type = 'article';
    http_response_code($Error404->code);
    $path->content = $Error404;
  }

  $path->type = $type;

  pushAlert('Current Path type: '. $type,'info');

  return $path;
}

 ?>
