<?php
http_response_code(200);
header('Content-type: text/plain');

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
    $baseurl= "https";
}
else{
    $baseurl = "http";
}

if ($site->forcehttps){
  $baseurl= "https";
}

$baseurl .= "://";
$baseurl .= $_SERVER['HTTP_HOST'];

if ($site->installdir != ''){
  $baseurl .= '/'.$site->installdir;
}

echo "User-agent: *\n";
echo "Sitemap: $baseurl/sitemap.xml\n";
echo "Host: $baseurl\n";
?>
