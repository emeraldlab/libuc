<?php
http_response_code(200);
header('Content-type: application/xml');

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
    $baseurl= "https";
}
else{
    $baseurl = "http";
}

if ($site->forcehttps){
  $baseurl= "https";
}

$baseurl .= "://";
$baseurl .= $_SERVER['HTTP_HOST'];

if ($site->installdir != ''){
  $baseurl .= '/'.$site->installdir;
}

$website_lastmodified = checkUpdates($site)->website->changed;

echo '<?xml version="1.0" encoding="UTF-8"?>';
echo "\n";
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
echo "\n";
echo "<url><loc>".$baseurl."</loc><lastmod>".$website_lastmodified."</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>\n";
echo "\n";
foreach ($SitePages as $page) {
  if($page->noindexnofollow == 0){
  echo "<url><loc>".$baseurl.$page->path."</loc><lastmod>".$page->changed."</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>\n";
  }
}
foreach ($SiteTerms as $term) {
  echo "<url><loc>".$baseurl."/".$term->path."</loc><lastmod>".$website_lastmodified."</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>\n";
}
foreach ($SiteArticles as $article) {
  echo "<url><loc>".$baseurl.$article->path."</loc><lastmod>".$article->changed."</lastmod><changefreq>weekly</changefreq><priority>0.5</priority></url>\n";
}

// <url><loc>https://adeglas.it/it/shop/portafoto-tasca</loc><lastmod>2018-01-21T21:50Z</lastmod><changefreq>never</changefreq></url>


echo '</urlset>';
echo "\n";

?>
