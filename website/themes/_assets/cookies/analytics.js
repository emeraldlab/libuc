$(window).on('load', function() {
  cookieBanner();
});


$('body').on("click tap", '.acceptCookies', function() {
  //console.log('hai cliccato su accetta!');
  $('#cookieBanner').fadeOut();
  Cookies.set('acceptCookies', 1, {
    expires: 7
  });
  cookieBanner();
});

$('body').on("click tap", '.refuseCookies', function() {
  //console.log('hai cliccato su rifiuta!');
  $('#cookieBanner').fadeOut();
  Cookies.set('acceptCookies', 0, {
    expires: 7
  });
  cookieBanner();
});

$('body').on("click tap", '#showcookiebanner', function() {
  $('#cookieBanner').fadeIn();
});


function cookieBanner() {

  acceptCookies = Cookies.get('acceptCookies');

  if (acceptCookies == 1) {
    $('#cookieBanner').css('display', 'none');
    $.getScript("/jshead", function() {});
  }
  else if (acceptCookies == 0) {
    $('#cookieBanner').css('display', 'none');
    deleteCookies();
    Cookies.set('acceptCookies', 0, {
      expires: 7
    });

  } else {
    //console.log('il cookiebanner non esiste');
  }
}

function deleteCookies() {
    var allCookies = document.cookie.split(';');

    var currentDomain = document.domain;
    var currentDomainArray = currentDomain.split('.');
    var currentDomain2 = '.'+currentDomainArray.at(-2)+'.'+currentDomainArray.at(-1);


    // The "expire" attribute of every cookie is
    // Set to "Thu, 01 Jan 1970 00:00:00 GMT"
    for (var i = 0; i < allCookies.length; i++){
        document.cookie = allCookies[i] + "=;expires=" + new Date(0).toUTCString();
        document.cookie = allCookies[i] + '=; Path=/; domain='+currentDomain+'; expires=' + new Date(0).toUTCString();
        document.cookie = allCookies[i] + '=; Path=/; domain='+currentDomain2+'; expires=' + new Date(0).toUTCString();
      }

      var cookiesAfter = document.cookie.split(";");
      console.log('Cookies dopo la rimozione');
      console.log(cookiesAfter);

}
