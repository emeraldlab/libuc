<?php

if ($site->GTMid){
  $AnalyticsGtmHead->id = $site->GTMid;
  $AnalyticsGtmHead->rendered = "
\n\n
/* GTM inclusion script - id: ".$AnalyticsGtmHead->id." */
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','".$AnalyticsGtmHead->id."');";
}


if ($site->GAid){
  $AnalyticsGaHead->id = $site->GAid;
  $AnalyticsGaHead->rendered = "
\n\n
/* GA inclusion script - id: ".$AnalyticsGaHead->id." */
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','".$AnalyticsGtmHead->id."');

window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', '".$AnalyticsGtmHead->id."');
";
}

if ($site->MTMid){
  $AnalyticsMatomoScript->id = $site->MTMid;
  /*$AnalyticsMatomoScript->endpoint = $site->MTMendpoint;*/
  $AnalyticsMatomoScript->endpoint = 'https://matomo.emtools.it/';
  $AnalyticsMatomoScript->rendered = "
\n\n
/* Mataomo inclusion script - id: ".$AnalyticsMatomoScript->id."*/
var _paq = window._paq = window._paq || [];
/* tracker methods like 'setCustomDimension' should be called before 'trackPageView' */
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
  var u='".$AnalyticsMatomoScript->endpoint."';
  _paq.push(['setTrackerUrl', u+'matomo.php']);
  _paq.push(['setSiteId', '".$AnalyticsMatomoScript->id."']);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
  g.async=true; '".$AnalyticsMatomoScript->endpoint."matomo.js'; s.parentNode.insertBefore(g,s);
})();
  ";
}

if ($site->MTMtag){
  $AnalyticsMatomoContainer->id = $site->MTMtag;
  //$AnalyticsMatomoContainer->endpoint = $site->MTMendpoint;
  $AnalyticsMatomoContainer->endpoint = 'https://matomo.emtools.it/';
  $AnalyticsMatomoContainer->rendered = "
\n\n
/* Mataomo TAG inclusion script - id: ".$AnalyticsMatomoContainer->id."*/
var _mtm = window._mtm = window._mtm || [];
_mtm.push({'mtm.startTime': (new Date().getTime()), 'event': 'mtm.Start'});
var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
g.async=true; g.src='".$AnalyticsMatomoContainer->endpoint."js/container_".$AnalyticsMatomoContainer->id.".js'; s.parentNode.insertBefore(g,s);";
}



?>
