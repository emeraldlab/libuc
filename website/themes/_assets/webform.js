jQuery(function($){


  // Webform variables.
  var WebformEndPoint = $("meta[name='webformendpoit']").attr("content");
  var WebformID =  $("meta[name='webformid']").attr("content");
  var WebformWrapper = '#webformwrapper';
  var SendSMS01 = '';
  var HoneypotTimemout = 5;


  var debug = $("meta[name='webformdebug']").attr("content");
  var feedback =  $("meta[name='webformokfeedback']").attr("content");
  var redirect = $("meta[name='webformredirect']").attr("content");
  var messageok = $("meta[name='webformmessageok']").attr("content");
  var messageko = $("meta[name='webformmessageko']").attr("content");

 $( document ).ready(function() {

   if (WebformID){
     getCiviCrmForm(WebformEndPoint,WebformID,WebformWrapper,SendSMS01);
     checkRequiredFields(WebformWrapper);
   }

   var timeElapsed = false;
    setTimeout(() => {

     if (debug){ console.log('elapsed time:',HoneypotTimemout*1000);}
     timeElapsed = true;

   }, HoneypotTimemout*1000);

   $( "body" ).on( "click tap", ".webformPostMeNow", function() {
     var wrapper_id = $(this).parents('form').attr('id');
     wrapper_id = '#'+wrapper_id;

     var honeypot = $(wrapper_id).find('.customer_note input').val();
     if (debug){ console.log('honeypot:',honeypot);}

     if (honeypot == ''){
       if (timeElapsed){
         postCiviCrmForm(wrapper_id);
         $(wrapper_id).html('<div class="lds-ring"><div></div><div></div><div></div><div></div></div>');
       }else{
         event.preventDefault();
         if (debug){ console.log('Too Early!');}
       }

     }
     else{
       event.preventDefault();
       if (debug){ console.log('Noway!');}
     }
   });

   $(document).on('change','.webform_form input',function () {
     checkRequiredFields(WebformWrapper);
   });
});




function getCiviCrmForm(endpoint,WebformID,WebformWrapper,SendSMS){

 var endpointGet = endpoint+'webform_rest/'+WebformID+'/fields';
 //var endpointGet = endpoint+'webform_rest/'+WebformID+'/elements';
 var endpointPost = endpoint+'webform_rest/submit';
 //var endpointGet = 'https://crmdev.wikimedia.it/webform/webform_rest';
 if (debug){
   console.log('getCiviCrmForm - form id: '+WebformID);
   console.log('getCiviCrmForm - endpointGet: '+endpointGet);
   console.log('getCiviCrmForm - endpointPost: '+endpointPost);
 }

 // Assign handlers immediately after making the request,
 // and remember the jqxhr object for this request
 var jqxhr = $.get( endpointGet, function(data) {
    if (debug){
      if (debug){
        console.log( "ajax get success" );
      }
    }
   })
   .done(function(data) {
     //console.log( "second success",data );
     renderCiviCrmForm(data,WebformID,endpointPost,WebformWrapper,SendSMS);
   })
   .fail(function(data) {
     if (debug){
       console.log("error: ",data );
    }
   })
   .always(function(data) {
     if (debug){
       console.log("finished: ",data );
     }
   });

 };


 function renderCiviCrmForm(data,WebformID,endpointPost,WebformWrapper,SendSMS){

   var tokenValue =  $("meta[name='validationtoken']").attr("content");
   if (debug){
     console.log('tokenValue',tokenValue);
     console.log('renderCiviCrmForm');
   };

   output = "";
   output += "<form id='webform-"+WebformID+"' class='webform_form' data-post-url='"+endpointPost+"' data-form-id='"+WebformID+"' data-sendsms='"+SendSMS+"'>";
   $.each( data, function( key, value ) {

     if (value['#type'] == 'checkboxes'){
       typeclass= ' multiplevalues fieldtype_checkboxes';
     }
     else if (value['#type'] == 'select'){
       typeclass= ' singlevalue fieldtype_select';
     }
     else if (value['#type'] == 'radios'){
       typeclass= ' singlevalue fieldtype_radio';
     }
     else if (value['#type'] == 'checkbox'){
       typeclass= ' singlevalue fieldtype_checkbox';
     }
     else if (value['#type'] == 'textarea'){
       typeclass= ' singlevalue fieldtype_textarea';
     }
     else{
       typeclass= ' singlevalue';
     }

     console.log(typeclass);

     output += "<div class='form-item form-group "+value['#webform_key']+typeclass+"'>";
     output += renderCiviCrmField(value,WebformID);
     output += "</div>";

   });
   output += "<button class='webformPostMeNow btn btn-primary'>Invia</button>";

   output += "</form>";

  if (debug){
    console.log(output);
  }

   $(WebformWrapper).html(output);
   checkRequiredFields(WebformWrapper);
 };

 function renderCiviCrmField(field,WebformID){
   if (debug){
     console.log(field);
   }
   is_required = '';
   if (field['#required'] == true){
     is_required = "required='required' required";
   }

   if (field['#placeholder'] == undefined){
     field['#placeholder'] = field['#title'];
   }

   output = '';

   if (field['#type'] == 'email'){
     output += "<label>"+field['#title'];
     if (field['#required'] == true){ output += ' <span class="required">*</span>' };
     output += "</label>";
     if (field['#description']){
     output += "<div class='form_description'>"+field['#description']+"</div>";
     }
     output += "<input class='form-control field input' type='email' name='"+field['#webform_key']+"' placeholder='"+field['#placeholder']+"' "+is_required+">";
     output += "<div class='error'></div>";
   }

   if (field['#type'] == 'textfield'){
     output += "<label>"+field['#title'];
     if (field['#required'] == true){ output += ' <span class="required">*</span>' };
     output += "</label>";
     if (field['#description']){
     output += "<div class='form_description'>"+field['#description']+"</div>";
     }
     output += "<input class='form-control field input' type='text' name='"+field['#webform_key']+"' placeholder='"+field['#placeholder']+"' "+is_required+">";
     output += "<div class='error'></div>";
   }


   if (field['#type'] == 'textarea'){
     output += "<label>"+field['#title'];
     if (field['#required'] == true){ output += ' <span class="required">*</span>' };
     output += "</label>";
     if (field['#description']){
     output += "<div class='form_description'>"+field['#description']+"</div>";
     }
     output += "<textarea class='form-control' name='"+field['#webform_key']+"' id='"+field['#webform_key']+"' "+is_required+"></textarea>";
     output += "<div class='error'></div>";
   }



   if (field['#type'] == 'hidden'){
     output += "<input class='field input' type='hidden' name='"+field['#webform_key']+"' placeholder='"+field['#placeholder']+"' "+is_required+">";
   }

   if (field['#type'] == 'civicrm_contact'){
     output += "<input class='form-control field input' type='hidden' name='"+field['#webform_key']+"' placeholder='"+field['#placeholder']+"' "+is_required+">";
   }

   if (field['#type'] == 'checkbox'){
     output += "<div class='checkboxwrapper'>";
     output += "<label>"+field['#title'];
     if (field['#required'] == true){ output += ' <span class="required">*</span>' };
     output += "</label>";

     output += "<div class='form-check'>";
     output += "<label class='form-check-label' for='"+field['#webform_key']+"'>"+field['#description']+"</label>";
     output += "<input class='form-check-input' id='"+field['#webform_key']+"' name='"+field['#webform_key']+"' type='checkbox' value='1' "+is_required+">";
     output += "</div>";

     output += "<div class='error'></div>";
     output += "</div>";
   }


   if (field['#type'] == 'select'){
    output += "<div class='checkboxwrapper'>";
    output += "<label>"+field['#title'];
    if (field['#required'] == true){ output += ' <span class="required">*</span>' };
    output += "</label>";
    if (field['#description']){
      output += "<div class='form_description'>"+field['#description']+"</div>";
    }
    output += "<select class='form-select' name='"+field['#webform_key']+"'"+is_required+">";
     $.each(field['#options'], function (key,value) {

        output += "<option value='"+key+"'>"+value+"</option>";

     });
     output += "</select>";
     output += "<div class='error'></div>";
     output += "</div>";

   }


   if (field['#type'] == 'checkboxes'){
    output += "<div class='selectwrapper'>";
    output += "<label>"+field['#title'];
    if (field['#required'] == true){ output += ' <span class="required">*</span>' };
    output += "</label>";
    if (field['#description']){
    output += "<p class='checkboxes_description'>"+field['#description']+"</p>";
    }
     $.each(field['#options'], function (key,value) {
        output += "<div class='form-check'>";
        output += "<label class='form-check-label' for='"+field['#webform_key']+key+"'>"+value+"</label>";
        output += "<input class='form-check-input' id='"+field['#webform_key']+key+"' name='"+field['#webform_key']+"' type='checkbox' value='"+key+"' "+is_required+">";
        output += "</div>";
     });
     output += "<div class='error'></div>";
     output += "</div>";

   }

   if (field['#type'] == 'radios'){
    output += "<div class='selectwrapper'>";
    output += "<label>"+field['#title'];
    if (field['#required'] == true){ output += ' <span class="required">*</span>' };
    output += "</label>";
    if (field['#description']){
    output += "<p class='radios_description'>"+field['#description']+"</p>";
    }
     $.each(field['#options'], function (key,value) {
        output += "<div class='form-check'>";
        output += "<input class='form-check-input' id='"+field['#webform_key']+key+"' name='"+field['#webform_key']+"' type='radio' value='"+key+"' "+is_required+">";
        output += "<label class='form-check-label' for='"+field['#webform_key']+key+"'>"+value+"</label>";
        output += "</div>";
     });
     output += "<div class='error'></div>";
     output += "</div>";

   }

   if (field['#type'] == 'webform_actions'){
     output += "<button class='btn webformPostMeNow' type='button'>"+field['#submit__label']+"</button>";

   }

   return output;

 };

 function postCiviCrmForm(WebformWrapper){

   var endpointPost = $(WebformWrapper).attr('data-post-url');

   var form_id = $(WebformWrapper).attr('data-form-id');
   var jsonArray = {};
   if (debug){
     console.log('endpointPost',endpointPost);
     console.log('form_id',WebformWrapper);
  }

   jsonArray['webform_id'] = form_id;
   if (debug){console.log('jsonObj',jsonArray);}

   $( WebformWrapper+" .form-item" ).each(function( ) {

     if ($(this).hasClass('multiplevalues')){
       var values = [];

       i = 0;
       $(this).find('input').each(function( ) {
         if ($(this).is(':checked')){
           newvalue = $(this).val();

           if (debug){console.log(newvalue);}

           if (newvalue.lenght != 0){
              values[i] = newvalue;
              i++;
           }

         }

       });

       //values = values.slice(0,-1);
       if (debug){console.log('multiplevalues values',values);}

       if (values){
         name = $(this).find('input').attr('name');
         value = values;
       }

       if (debug){ console.log('multiplevalues values',value);}
       if (debug){console.log($(this),'multiplevalues');}

     }
     else if ($(this).hasClass('fieldtype_select')){
      name = $(this).find('select').attr('name');
      value = $( "option:selected" ).val();
     }

     else if ($(this).hasClass('fieldtype_checkbox')){
      name = $(this).find('input:checked').attr('name');
      value = $(this).find('input:checked').val();
      }
     else if ($(this).hasClass('fieldtype_radio')){
      name = $(this).find('input:checked').attr('name');
      value = $(this).find('input:checked').val();
    }

    else if ($(this).hasClass('fieldtype_textarea')){
     name = $(this).find('textarea').attr('name');
     value = $(this).find('textarea').val();

    }
    else{

     if (debug){console.log($(this),'single value');}
     name = $(this).find('input').attr('name');
     value = $(this).find('input').val();
     }

     if ( name != 'undefined'){
       jsonArray[name] = value;
     }else{
       if (debug){console.log('nessun valore');}
     }
   });



   if (debug){
     console.log('jsonArray',jsonArray);
   }

   json = JSON.stringify(jsonArray);
   //$(WebformWrapper).append(json);

   var request = $.ajax({
     url: endpointPost,
     headers: {
       'Accept': 'application/json',
       'Content-Type': 'application/json'
     },
     method: "POST",
     dataType: "json",
     data: json,
   });

   request.done(function( msg ) {
     if (debug){
       console.log('done',msg);
       console.log('feedback: ',feedback);
       console.log('messageok: ',messageok);

    }

    if (feedback == 'message'){
     $(WebformWrapper).html(messageok);
    }
    else if (feedback == 'redirect') {
     window.location.replace(redirect);
    }
    else{
      $(WebformWrapper).html('<h2>Default Grazie!</h2><h3>Verrai contattato al più presto dal nostro team.</h3>');
    }

   });

   request.fail(function( jqXHR, textStatus ) {
     if (debug){
       console.log( "Request failed: " + textStatus );
       console.log( "Request failed: " + jqXHR );
    }
    $(WebformWrapper).html(messageko);
    //window.location.replace("/pages/grazie");
   });

 }

 function checkRequiredFields(form) {

   $( form ).each(function( index ) {
     if (debug){
       var formClass = $(this).attr('class');
       console.log('checkRequiredFields - Class: ',formClass);
       console.log('checkRequiredFields - Argument',form);
     }

     $(this).find('button.webformPostMeNow').addClass('disabled');
     $(this).find('button.webformPostMeNow').prop("disabled",true);

     var can_send = true;
     var allRequiredFields = [];

     $(this).find('input').each(function() {

       if ($(this).prop('required')){
         name = $(this).attr('name');
         type = $(this).attr('type');
         value = $(this).val();

         if (debug){
           console.log('['+form+'] ['+type+'] '+name+': ', value);
         }

         $(this).parents('.form-item').find('.error').html('');

         if (type=='checkbox'){
           if ($(this).is(":checked"))
             {
               $(this).parents('.form-item').find('.error').html('');
             }
           else{
             $(this).parents('.form-item').find('.error').html('Questo campo è obbligatorio');
             can_send = false;
           }
         }

         if (value != null && value != '' ){
           allRequiredFields[name] = true;

           if (type=='email'){

             var EmailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
             if('regex',EmailRegex.test(value)){

             }
             else{
               $(this).parents('.form-item').find('.error').html('L\'indirizzo email inserito non è valido.');
               can_send = false;
             }

           }
         }
         else{
           allRequiredFields[name] = false;
           $(this).parents('.form-item').find('.error').html('Questo campo è obbligatorio');
           can_send = false;

         }
       }else{
         required = false;
       }

     });


     if (debug){
       console.log('cansend:', can_send);
     }

     if (can_send){
       $(this).find('button.webformPostMeNow').removeClass('disabled');
       $(this).find('button.webformPostMeNow').prop("disabled",false);
     }
   });



 }


});
