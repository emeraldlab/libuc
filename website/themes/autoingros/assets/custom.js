$(document).ready(function() {
  if ($('.page_home_wrapper').length) {
    AOS.init({
      duration: 1000,
      once: true
    });
  }

  if ($('.page_wrapper').length) {
    AOS.init({
      duration: 1000,
      once: true
    });
  }
});
