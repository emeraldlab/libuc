$(window).on('load', function() {
  hideLoader();
});

function hideLoader() {
  $('#loader_out').delay(500).fadeOut(500);
}


$(window).scroll(function() {
  if ($('#numbers_section').length) {
    startCounter();
  }
});

var executeCounter = 0;

function startCounter() {
  var stop = 0;
  var wH = $(window).height();
  var currentTop = $(window).scrollTop();
  stop = $("#numbers_section").offset().top - (wH * 1.1);

  if (currentTop > stop) {
    if (executeCounter == 0) {
      // console.log('Devo partire');
      executeCounter = 1;

      $('.number').each(function() {
        var $this = $(this);
        var counterValue = parseInt($this.text());
        jQuery({
          Counter: 0
        }).animate({
          Counter: counterValue,
          complete: function() {
            $this.text(Math.ceil(this.Counter).toLocaleString());
          }
        }, {
          duration: 3000,
          easing: 'swing',
          step: function() {
            $this.text(Math.ceil(this.Counter).toLocaleString());
          }
        });
      });

    }

  }
}
