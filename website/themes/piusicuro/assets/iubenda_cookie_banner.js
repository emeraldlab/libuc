
  var _iub = _iub || [];
  _iub.csConfiguration = {
    "askConsentAtCookiePolicyUpdate":true,
  	"countryDetection":true,
  	"enableFadp":true,
  	"enableRemoteConsent":true,
  	"enableTcf":true,
  	"floatingPreferencesButtonDisplay":"bottom-right",
  	"googleAdditionalConsentMode":true,
  	"perPurposeConsent":true,
  	"siteId":3285694,
  	"tcfPurposes":{
  		"2":"consent_only",
  		"7":"consent_only",
  		"8":"consent_only",
  		"9":"consent_only",
  		"10":"consent_only",
  		"11":"consent_only"
  	},
  	"whitelabel":false,
  	"cookiePolicyId":89868197,
  	"lang":"it",
  	"banner":{
  		"acceptButtonDisplay":true,
  		"closeButtonRejects":true,
  		"customizeButtonDisplay":true,
  		"explicitWithdrawal":true,
  		"listPurposes":true,
  		"position":"float-bottom-center",
  		"prependOnBody":true,
  		"rejectButtonDisplay":true,
  		"showPurposesToggles":true
  	},
    callback: {
                onPreferenceExpressedOrNotNeeded: function(preference) {
                    dataLayer.push({
                        iubenda_ccpa_opted_out: _iub.cs.api.isCcpaOptedOut()
                    });
                    if (!preference) {
                        dataLayer.push({
                            event: "iubenda_preference_not_needed"
                        });
                    } else {
                        if (preference.consent === true) {
                            dataLayer.push({
                                event: "iubenda_consent_given"
                            });
                        } else if (preference.consent === false) {
                            dataLayer.push({
                                event: "iubenda_consent_rejected"
                            });
                        } else if (preference.purposes) {
                            for (var purposeId in preference.purposes) {
                                if (preference.purposes[purposeId] && ("_iub_cs-89868197" === undefined || "_iub_cs-89868197".indexOf('"' + purposeId + '":true') < 0)){
                                    dataLayer.push({
                                        event: "iubenda_consent_given_purpose_" + purposeId
                                    });
                                }
                            }
                        }
                    }
                }
            }


  };
