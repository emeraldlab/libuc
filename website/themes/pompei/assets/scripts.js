

$(window).on('load', function() {
  // videoPlayer();
});



function videoPlayer() {
    $('.video_wrapper').append('<div class="playme"><div class="playme_inner"><div class="playme_icon"><img id="logoplay" src="/themes/pompei/media/play.png"></div></div></div>');

    $('.playme').click(function() {
      {
        // console.log('clicked');
        $(this).toggleClass('clicked');
        var currentVideo = $(this).parents('.video_wrapper').find('video').get(0);
        if (currentVideo.paused) {
          currentVideo.play();
          $(".playme_icon", this).fadeOut();
        } else {
          currentVideo.pause();
          $(".playme_icon", this).fadeIn();
        }
      }
    });
  }
